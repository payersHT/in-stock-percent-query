--Setting Vars for query
DECLARE @LW varchar(30);
DECLARE @week_num varchar(30);
DECLARE @AIR_filter INT;
SET @AIR_filter = 10


(SELECT @LW = week
    FROM SizingAnalysis.dbo.dim_time
    where (day_dt+7) in (SELECT CAST(getdate()  AS Date)));

(SELECT @week_num = week
	FROM SizingAnalysis.dbo.dim_time
    where (day_dt) in (SELECT CAST(getdate()  AS Date)));


SELECT DISTINCT STORE_NUM
INTO #store_num
FROM SizingAnalysis.dbo.store_master
WHERE ACTIVE = 'Y'
AND Alloc_Active = 'Y'
AND COMPANY = 'HTC'
AND STORE_NUM < 2000

SELECT prod.Master_Style,
		SUM(unit_sales) total_sales,
		RANK() OVER (ORDER BY sum(unit_sales)desc) 'Rank_number'
INTO #rank_styles_temp
FROM SizingAnalysis.dbo.fact_sales sls
JOIN SizingAnalysis.dbo.dim_product prod
	ON prod.sku_number = sls.sku_number
where fiscalweek = @LW
AND store_number IN (SELECT STORE_NUM FROM #store_num)
GROUP by sls.style_number,prod.Master_Style,sls.sku_number
ORDER BY total_sales desc , prod.master_style;


SELECT DISTINCT init_a.Master_Style,
init_a.Rank_number,
int_b.sku_number,
int_b.sku_description,
int_b.department,
int_b.class,
int_b.style_description parent_style_desc
INTO #rank_styles
FROM #rank_styles_temp init_a 
JOIN SizingAnalysis.dbo.dim_product int_b
ON init_a.master_style = int_b.style_number
WHERE Rank_number <= 500
ORDER BY Rank_number;

SELECT *
into #inv
FROM SizingAnalysis.dbo.fact_inventory
WHERE store_number in (SELECT DISTINCT store_number FROM #store_num)
AND fiscalweek = @week_num
AND sku_number IN (SELECT DISTINCT sku_number FROM #rank_styles )

SELECT sls.store_number,
RS.Master_Style,
RS.parent_style_desc,
RS.sku_number,
RS.Rank_number,
RS.department,
RS.class,
sls.unit_sales AS 'LW Sales',
inv.unit_eop AS 'TW Inventory',
inv.r_eop,
INV.r_eop/NULLIF(unit_eop,0) AS AIR,
@AIR_filter as AIR_FILTER
FROM SizingAnalysis.dbo.fact_sales sls
JOIN #rank_styles RS
	ON RS.sku_number = sls.sku_number
JOIN #inv inv
	ON inv.sku_number = RS.sku_number
	AND inv.store_number = sls.store_number
WHERE sls.sku_number IN (SELECT DISTINCT sku_number FROM #rank_styles)
AND sls.store_number IN (SELECT DISTINCT STORE_NUM  FROM #store_num )
AND sls.fiscalweek = @LW
order by Rank_number , store_number

DROP TABLE #rank_styles_temp
DROP TABLE #rank_styles
DROP TABLE #store_num
DROP Table #inv