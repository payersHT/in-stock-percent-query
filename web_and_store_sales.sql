--Getting Safty Stock Numbers
DECLARE @store_safty_stock INT;
DECLARE @web_safty_stock INT;
DECLARE @AIR_filter INT;SET @store_safty_stock = 1;
SET @web_safty_stock = 3;
SET @AIR_filter =12;

--Getting the weeknumber to be used to filter
DECLARE @week_num varchar(30)
   (SELECT @week_num = week
    FROM SizingAnalysis.dbo.dim_time
    where day_dt in (SELECT CAST(getdate()  AS Date)));

--Getting the style list
SELECT DISTINCT department,class, sku_number
INTO #style_list
FROM SizingAnalysis.dbo.dim_product
WHERE division = '1'
AND department NOT IN ('27','35','36','38','80','99')
AND class_name NOT LIKE '%Dummy%'
AND sku_number  IN (SELECT DISTINCT sku_number 
                    FROM SizingAnalysis.dbo.fact_inventory 
                    where fiscalweek = @week_num 
                    AND unit_eop >= @store_safty_stock
                    --check to see if can be adjusted to reg_eop
                    AND (r_eop/reg_unit_eop) >= @AIR_filter);


--Getting the store list for HT
SELECT DISTINCT STORE_NUM
INTO #store_list
FROM SizingAnalysis.dbo.store_master
WHERE LOC_TYPE = 'STR'
AND COMPANY = 'HTC'
AND ACTIVE = 'y';


SELECT DISTINCT store_number,
    STL.department,
    STL.class,
    COUNT(*) as Total_Style_count,
 SUM(TINV.reg_unit_eop) AS Available_store_units
INTO #Total_table
FROM SizingAnalysis.dbo.fact_inventory AS TINV
JOIN #style_list AS STL
 ON STL.sku_number = TINV.sku_number
where fiscalweek = @week_num
 and store_number IN (SELECT DISTINCT STORE_NUM FROM #store_list)
 and TINV.sku_number IN (SELECT DISTINCT sku_number FROM #style_list)
Group by store_number,STL.department,STL.class 
ORDER by store_number, department,class;



SELECT DISTINCT store_number,
    STL.department,
    STL.class,
    COUNT(*)  as Over_lap_count,
 SUM(OINV.reg_unit_eop) AS Available_web_units
INTO #overlap_table
FROM SizingAnalysis.dbo.fact_inventory OINV
JOIN #style_list AS STL
 ON STL.sku_number = OINV.sku_number
where fiscalweek = @week_num
 and store_number IN (SELECT DISTINCT STORE_NUM FROM #store_list)
and OINV.sku_number IN (SELECT DISTINCT sku_number
         FROM SizingAnalysis.dbo.fact_inventory
          WHERE fiscalweek = @week_num
         and store_number IN ('4490','4499')
         and sku_number IN (SELECT DISTINCT sku_number FROM #style_list)
         and reg_unit_eop >= @web_safty_stock)
Group by store_number,STL.department,STL.class
ORDER BY store_number, department,class;

SELECT PROD.department,
    PROD.class,
    INV.store_number,
    count(inv.sku_number) AS 'Total SKU Count For Stores',
    SUM(INV.reg_unit_eop) AS 'Total OH Inventory For Stores'
INTO #total_inv_table
FROM SizingAnalysis.dbo.fact_inventory INV
JOIN (SELECT DISTINCT department,class, sku_number
   FROM SizingAnalysis.dbo.dim_product
   WHERE division = '1'
   AND department NOT IN ('27','35','36','38','80','99')
   AND class_name NOT LIKE '%Dummy%') PROD
ON PROD.sku_number = INV.sku_number 
WHERE fiscalweek = @week_num
AND INV.reg_unit_eop > 0 
AND store_number IN (SELECT DISTINCT STORE_NUM FROM #store_list)
GROUP by PROD.department,prod.class,inv.store_number
ORDER BY inv.store_number,
  PROD.department,
  PROD.class

--adjust to reg inv
SELECT TT.store_number,
TT.department,
TT.class,
ISNULL(OT.Available_web_units,0) AS 'Available_web_units',
ISNULL(TT.Available_store_units,0) AS 'Available_store_units',
ISNULL(TT.Total_Style_count,0) AS 'Total Style Count',
ISNULL(OT.Over_lap_count,0) AS 'Overlap Stlye Count',
ISNULL(TIT.[Total OH Inventory For Stores],0) AS 'Total OH Inventory For Stores',
ISNULL(TIT.[Total SKU Count For Stores],0) AS 'Total SKU Count For Stores'
FROM #Total_table TT
FULL JOIN #overlap_table OT
ON TT.store_number = OT.store_number
AND TT.department = OT.department
AND TT.class = OT.class
JOIN #total_inv_table TIT
ON TIT.store_number = TT.store_number
AND TIT.department = TT.department
AND TIT.class = TT.class
ORDER by TT.store_number,
         TT.department,
         TT.class,
         OT.Available_web_units,
         TT.Available_store_units


DROP TABLE #overlap_table
DROP TABLE #store_list
DROP TABLE #style_list
DROP TABLE #Total_table
DROP TABLE #total_inv_table