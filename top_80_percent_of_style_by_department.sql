--Setting Vars for query
DECLARE @LW varchar(30);
DECLARE @week_num varchar(30);
DECLARE @AIR_filter INT;
SET @AIR_filter = 10


(SELECT @LW = week
    FROM SizingAnalysis.dbo.dim_time
    where (day_dt+7) in (SELECT CAST(getdate()  AS Date)));

(SELECT @week_num = week
	FROM SizingAnalysis.dbo.dim_time
    where (day_dt) in (SELECT CAST(getdate()  AS Date)));


SELECT DISTINCT STORE_NUM
INTO #store_num
FROM SizingAnalysis.dbo.store_master
WHERE ACTIVE = 'Y'
AND Alloc_Active = 'Y'
AND COMPANY = 'HTC'
AND STORE_NUM < 2000

SELECT DISTINCT prod.department as department,
sls.style_number,
sls.sku_number,
sum(regpro_unit_sales) OVER (partition by prod.department,sls.sku_number) AS TOTAL_by_SKU,
SUM(regpro_unit_sales) OVER (partition by prod.department) AS TOTAL_BY_DEPARTMENT
INTO #running_total
FROM SizingAnalysis.dbo.fact_sales sls
JOIN SizingAnalysis.dbo.dim_product prod
 ON prod.sku_number = sls.sku_number
where regpro_unit_sales > 0
AND prod.division = '1'
AND prod.department NOT IN ('27','35','36','38','80','99')
AND prod.class_name NOT LIKE '%Dummy%'
and sls.fiscalweek = @LW
and sls.store_number IN (SELECT STORE_NUM FROM #store_num)
order by prod.department, TOTAL_by_SKU DESC


SELECT department,
style_number,
sku_number,
TOTAL_by_SKU,
TOTAL_BY_DEPARTMENT,
SUM(TOTAL_by_SKU/TOTAL_BY_DEPARTMENT) OVER(PARTITION BY department
             ORDER BY TOTAL_by_SKU DESC
             ROWS UNBOUNDED PRECEDING)as PERCENT_TO_TOTAL
INTO #rank_styles
FROM #running_total
order by department,TOTAL_by_SKU DESC

SELECT *
into #inv
FROM SizingAnalysis.dbo.fact_inventory
WHERE store_number in (SELECT DISTINCT store_number FROM #store_num)
AND fiscalweek = @week_num
AND sku_number IN (SELECT DISTINCT sku_number FROM #rank_styles )

SELECT sls.store_number,
prod.Master_Style,
prod.style_number,
prod.style_description,
RS.sku_number,
RS.department,
prod.class,
sls.unit_sales AS 'LW Sales',
inv.unit_eop AS 'TW Inventory',
inv.r_eop,
INV.r_eop/NULLIF(unit_eop,0) AS AIR,
@AIR_filter as AIR_FILTER,
RS.TOTAL_BY_DEPARTMENT,
RS.PERCENT_TO_TOTAL
FROM SizingAnalysis.dbo.fact_sales sls
JOIN #rank_styles RS
 ON RS.sku_number = sls.sku_number
JOIN #inv inv
 ON inv.sku_number = RS.sku_number
 AND inv.store_number = sls.store_number
JOIN SizingAnalysis.dbo.dim_product prod 
	ON sls.sku_number = prod.sku_number
WHERE sls.sku_number IN (SELECT DISTINCT sku_number FROM #rank_styles)
AND sls.store_number IN (SELECT DISTINCT STORE_NUM  FROM #store_num )
AND sls.fiscalweek = @LW
AND RS.PERCENT_TO_TOTAL <=0.80

drop table #running_total
DROP TABLE #rank_styles
DROP TABLE #store_num
DROP Table #inv
